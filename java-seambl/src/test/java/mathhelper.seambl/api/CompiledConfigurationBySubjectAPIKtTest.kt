package mathhelper.seambl.api

import mathhelper.seambl.config.ComparisonType
import mathhelper.seambl.expressiontree.ExpressionSubstitution
import mathhelper.seambl.expressiontree.NodeType
import mathhelper.seambl.expressiontree.addRootNodeToExpression
import org.junit.Test

import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class CompiledConfigurationBySubjectAPIKtTest {

    @Test
    fun testCorrectionOfDefinedRules() {
        val compiledConfiguration = createConfigurationFromRulePacksAndParams(rulePacksMap.keys.toTypedArray())
        var incorrectRulesCount = 0
        for (rule in compiledConfiguration.compiledExpressionTreeTransformationRules) {
            if (rule.left.nodeType == NodeType.ERROR || rule.right.nodeType == NodeType.ERROR) {
                incorrectRulesCount++
                println("Syntax error while parsing '${rule.left}' -> '${rule.right}'")
            } else if (rule.left.nodeType != NodeType.EMPTY || rule.right.nodeType != NodeType.EMPTY) {
                if (!rule.basedOnTaskContext) {
                    if (!compiledConfiguration.factComporator.expressionComporator.fastProbabilityCheckOnIncorrectTransformation(addRootNodeToExpression(rule.left), addRootNodeToExpression(rule.right), ComparisonType.EQUAL)) {
                        incorrectRulesCount++
                        println("'${rule.left}' != '${rule.right}'")
                    }
                }
            }
        }
        assertTrue(incorrectRulesCount <= 25)
    }

    @Test
    fun createRuleCombinationPackConfiguration() {
        val compiledConfiguration = createConfigurationFromRulePacksAndParams(listOf("Combinatorics", "Logic", "LogicAbsorption", "LogicResolution", "PhysicsSimpleMoving", "PhysicsCircleMoving", "PhysicsNuclear", "PhysicsMolecular", "PhysicsElectrodynamics").toTypedArray())
        assertEquals(546, compiledConfiguration.compiledExpressionTreeTransformationRules.size)
        assertEquals(7, compiledConfiguration.compiledExpressionSimpleAdditionalTreeTransformationRules.size)
    }

    @Test
    fun createLogarithmRulePackConfiguration() {
        val compiledConfiguration = createConfigurationFromRulePacksAndParams(listOf("Logarithm").toTypedArray())
        assertEquals(169, compiledConfiguration.compiledExpressionTreeTransformationRules.size)
        assertEquals(7, compiledConfiguration.compiledExpressionSimpleAdditionalTreeTransformationRules.size)
    }
}