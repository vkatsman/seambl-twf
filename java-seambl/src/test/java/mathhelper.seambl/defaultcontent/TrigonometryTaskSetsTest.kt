package mathhelper.seambl.defaultcontent

import mathhelper.seambl.api.compareWithoutSubstitutions
import mathhelper.seambl.api.compareWithoutSubstitutionsStructureStrings
import mathhelper.seambl.api.stringToExpression
import mathhelper.seambl.api.structureStringToExpression
import mathhelper.seambl.config.CompiledConfiguration
import mathhelper.seambl.config.DebugOutputMessages
import mathhelper.seambl.config.FunctionConfiguration
import mathhelper.seambl.defaultcontent.defaulttasksets.allTrigonometryTasks
import mathhelper.seambl.defaultcontent.defaulttasksets.shortMultiplicationTrigonometryTasks
import mathhelper.seambl.expressiontree.ExpressionStructureConditionConstructor
import mathhelper.seambl.expressiontree.NodeType
import mathhelper.seambl.expressiontree.checkExpressionStructure
import org.junit.Ignore
import org.junit.Test
import mathhelper.seambl.assert
import kotlin.test.assertTrue

internal class TrigonometryTaskSetsTest {

    val debugOutputMessages = DebugOutputMessages(
            expressionProbabilityComparisonFalseDetailsPrintln = true
    )
    val expressionStructureConditionConstructor = ExpressionStructureConditionConstructor(CompiledConfiguration(functionConfiguration = FunctionConfiguration()))

    @Test
    fun allTrigonometryTasksCorrectness() {
        for (task in allTrigonometryTasks) {
            val originalExpression = structureStringToExpression(task.originalExpressionStructureString ?: "")
            assert(originalExpression.nodeType == NodeType.FUNCTION && originalExpression.value == "",
                    { "Error: originalExpressionStructureString '${task.originalExpressionStructureString}' is incorrect '${originalExpression.value}' " })

            if (!task.goalExpressionStructureString.isNullOrBlank()) {
                val goalExpression = structureStringToExpression(task.goalExpressionStructureString!!)
                assert(goalExpression.nodeType == NodeType.FUNCTION && goalExpression.value == "",
                        { "Error: originalExpressionStructureString '${task.goalExpressionStructureString}' is incorrect '${goalExpression.value}' " })

                assert(task.rules?.isNotEmpty() == true || compareWithoutSubstitutions(originalExpression, goalExpression, notChangesOnVariablesFunction = setOf(), debugOutputMessages = debugOutputMessages),
                        { "Error: '${task.originalExpressionStructureString}' != '${task.goalExpressionStructureString}'" } )
            }

            val hiddenGoalExpressionsStructureStrings = task.otherGoalData?.get("hiddenGoalExpressions") as List<String>?
            if (hiddenGoalExpressionsStructureStrings != null) {
                for (hiddenGoalExpressionsStructureString in hiddenGoalExpressionsStructureStrings) {
                    val hiddenGoalExpression = structureStringToExpression(hiddenGoalExpressionsStructureString)
                    assert(hiddenGoalExpression.nodeType == NodeType.FUNCTION && hiddenGoalExpression.value == "",
                            { "Error: originalExpressionStructureString '${hiddenGoalExpressionsStructureString}' is incorrect '${hiddenGoalExpression.value}' " })

                    assert(task.rules?.isNotEmpty() == true || compareWithoutSubstitutions(originalExpression, hiddenGoalExpression, notChangesOnVariablesFunction = setOf(), debugOutputMessages = debugOutputMessages),
                            { "Error: '${task.originalExpressionStructureString}' != '${hiddenGoalExpressionsStructureString}'" } )

                    if (task.goalPattern != null) {
                        val node = expressionStructureConditionConstructor.parse(task.goalPattern!!)
                        if(!checkExpressionStructure(hiddenGoalExpression, node)) {
                            println("Warning: goalPattern '${task.goalPattern}' does not match hiddenGoalExpression '${hiddenGoalExpressionsStructureString}'")
                        }
                    }
                }
            }
        }
    }

    @Test
    fun shortMultiplicationTrigonometryTasksTagStatisticTest() {
        val tagsSets = shortMultiplicationTrigonometryTasks.map { it.tags }
        val allTagsList = mutableListOf<String>()
        for (tagsSet in tagsSets) {
            for (tag in tagsSet) {
                allTagsList.add(tag)
            }
        }
        val historgam = allTagsList.groupBy { it }.map { Pair(it.key, it.value.size) }.sortedByDescending { it.second }
        val tagsMap = historgam.toMap()

        assertTrue(tagsMap["diffSqrs"]!! > 4)
//        assert(tagsMap["cubeDiff"]!! > 2)
        println(historgam.joinToString ("\n"))
    }
}