package mathhelper.seambl.trivialoffcheck

import mathhelper.seambl.api.checkChainCorrectnessInTex
import mathhelper.seambl.logs.log
import kotlin.test.assertEquals
import org.junit.Test

class Equality {
    @Test
    fun logArgumentsPlaces() {
        val result = checkChainCorrectnessInTex("\\textcolor{purple}{\\log _{16}4\\ \\textcolor{red}{=}\\ 0.5}")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\log _{16}4\\ \\textcolor{green}{=}\\ 0.5}",
                result.validatedSolution)
    }
}