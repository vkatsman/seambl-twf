package mathhelper.seambl.taskautogeneration

import mathhelper.seambl.api.*
import mathhelper.seambl.config.CompiledConfiguration
import mathhelper.seambl.expressiontree.ExpressionNode
import mathhelper.seambl.expressiontree.ExpressionSubstitution
import org.junit.Ignore
import org.junit.Test

class ExpressionGenerationKtTest {

    @Test
    @Ignore
    fun trigonometryShortMultiplicationStartExpressionGenerationTest() {
        val expressionsSet = mutableSetOf<ExpressionNode>()
        println(trigonometryShortMultiplicationStartExpressionGeneration(CompiledConfiguration()))


    }

    @Test
    @Ignore
    fun generateTrigonometryCompletions() {
        randomModifyExpression(stringToExpression("tg(x)*(sin(x)-cos(pi/2-x))"),
                getArithmeticDivisionSubstitutions() +
                getBasicTrigonometricDefinitionsIdentitySubstitutions(false) +
                        getTrigonometrySinCosSumReductionSubstitutions(false).apply { forEach { it.weightInTaskAutoGeneration = 100.0 } }
                        .filter {
                    !it.left.getContainedFunctions().contains("^") && !it.right.getContainedFunctions().contains("^")
                }
                , 7.6, 7.0
//                , listOf(
//                        expressionSubstitutionFromStructureStrings("(tg(x))", "(/(sin(x);cos(x)))"),
//                        expressionSubstitutionFromStructureStrings("(ctg(x))", "(/(cos(x);sin(x)))")
//                )
        )
    }

    fun randomModifyExpression(expression: ExpressionNode, substitutions: List<ExpressionSubstitution>,
                               difficultyCoef: Double = 5.0, varianceCoef: Double = 5.0,
                               mandatoryResultTransformations: List<ExpressionSubstitution> = listOf()) {
        val compiledConfiguration = CompiledConfiguration()
        val tasks = generateExpressionTransformationTasks(
                ExpressionTaskGeneratorSettings(
                        ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
                        difficultyCoef.toInt(),
                        (difficultyCoef * 0.75).toInt(),
                        { GeneratedExpression(expression) },
                        compiledConfiguration,
                        substitutions,
                        maxCountSelectedOfTasksOnIteration = varianceCoef.toInt() * 3,
                        widthOfRulesApplicationsOnIteration = (varianceCoef * 3).toInt(),
                        minStepsCountInAutogeneration = (difficultyCoef * 0.5).toInt(),
                        mandatoryResultTransformations = mandatoryResultTransformations
                )
        )

        tasks.forEach {
            println("${structureStringToString(it.goalExpressionStructureString!!)}   ->   '${it.goalExpressionStructureString}'")
        }
    }
}