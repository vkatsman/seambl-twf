package mathhelper.seambl

import org.junit.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

class JsSync {
    @Test
    fun sync () {
        syncSrc ()
    }

    fun syncSrc () {
        val javaMathhelperSeamblRoot = File("./src/main/java/mathhelper.seambl/")
        println(javaMathhelperSeamblRoot.canonicalPath)
        val jsMathhelperSeamblRoot = File("./../js-seambl/src/mathhelper.seambl/")
        println(jsMathhelperSeamblRoot.canonicalPath)
        javaMathhelperSeamblRoot.copyRecursively(jsMathhelperSeamblRoot, true)
    }
}