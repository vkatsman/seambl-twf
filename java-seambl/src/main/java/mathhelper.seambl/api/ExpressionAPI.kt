package mathhelper.seambl.api

import mathhelper.seambl.config.CompiledConfiguration
import mathhelper.seambl.expressiontree.*


fun normalizeExpressionToUsualForm(
        expression: ExpressionNode,
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration()
) {
    val topExpressionNode = expression.getTopNode()
    topExpressionNode.normalizeNullWeightCommutativeFunctions()
    topExpressionNode.reduceExtraSigns(setOf("+"), setOf("-"))
    topExpressionNode.normalizeSubtructions(compiledConfiguration.functionConfiguration)
    topExpressionNode.normalizeFunctionStringDefinitions(compiledConfiguration.functionConfiguration)
    topExpressionNode.normalizeNumbers()
    topExpressionNode.normalizeParentLinks()
    topExpressionNode.computeNodeIdsAsNumbersInDirectTraversalAndDistancesToRoot()
    topExpressionNode.computeIdentifier()
}