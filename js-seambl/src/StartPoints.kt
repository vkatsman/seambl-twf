import mathhelper.seambl.api.*
import mathhelper.seambl.config.*
import mathhelper.seambl.expressiontree.*
import mathhelper.seambl.logs.MessageType
import mathhelper.seambl.logs.log
import mathhelper.seambl.mainpoints.TexVerificationResult
import mathhelper.seambl.mainpoints.checkFactsInMathML
import mathhelper.seambl.mainpoints.configSeparator


//compiled configuration
@JsName("createCompiledConfigurationFromExpressionSubstitutionsAndParams")
fun createCompiledConfigurationFromExpressionSubstitutionsAndParams_JS(
        expressionSubstitutions: Array<ExpressionSubstitution>,
        additionalParamsMap: Map<String, String> = mapOf()
) = createCompiledConfigurationFromExpressionSubstitutionsAndParams(expressionSubstitutions, additionalParamsMap)

@JsName("createConfigurationFromRulePacksAndParams")
fun createConfigurationFromRulePacksAndParams_JS(
        rulePacks: Array<String> = listOf("Algebra").toTypedArray(),
        additionalParamsMap: Map<String, String> = mapOf()
) = createConfigurationFromRulePacksAndParams(rulePacks, additionalParamsMap)

@JsName("createConfigurationFromRulePacksAndDetailSolutionCheckingParams")
fun createConfigurationFromRulePacksAndDetailSolutionCheckingParams_JS(
        rulePacks: Array<String> = listOf("Algebra").toTypedArray(),
        wellKnownFunctionsString: String = "${configSeparator}0$configSeparator${configSeparator}1$configSeparator+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1$configSeparator^$configSeparator-1",
        expressionTransformationRulesString: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        maxExpressionTransformationWeight: String = "1.0",
        unlimitedWellKnownFunctionsString: String = wellKnownFunctionsString,
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        scopeFilter: String = "", //subject scopes which user representation sings is used
        wellKnownFunctions: List<FunctionIdentifier> = listOf(),
        unlimitedWellKnownFunctions: List<FunctionIdentifier> = wellKnownFunctions,
        expressionTransformationRules: List<ExpressionSubstitution> = listOf() //full list of expression transformations rules
) = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(
        rulePacks,
        wellKnownFunctionsString,
        expressionTransformationRulesString,
        maxExpressionTransformationWeight,
        unlimitedWellKnownFunctionsString,
        taskContextExpressionTransformationRules,
        maxDistBetweenDiffSteps,
        scopeFilter,
        wellKnownFunctions,
        unlimitedWellKnownFunctions,
        expressionTransformationRules
)

@JsName("getSubstitutionsByRulePacks")
fun getSubstitutionsByRulePacks_JS(rulePacks: Array<String>) = getSubstitutionsByRulePacks(rulePacks).toTypedArray()


//expressions
@JsName("stringToExpression")
fun stringToExpression_JS(
        string: String,
        scope: String = "",
        isMathMl: Boolean = false,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = stringToExpression(string, scope, isMathMl, functionConfiguration, compiledConfiguration)

@JsName("structureStringToExpression")
fun structureStringToExpression_JS(
        structureString: String,
        scope: String = "",
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        )
) = structureStringToExpression(structureString, scope, functionConfiguration)


@JsName("expressionToString")
fun expressionToString_JS(
        expressionNode: ExpressionNode,
        characterEscapingDepth: Int = 1
) = expressionToString(expressionNode, characterEscapingDepth)

@JsName("expressionToTexString")
fun expressionToTexString_JS(
        expressionNode: ExpressionNode,
        characterEscapingDepth: Int = 1
) = expressionToTexString(expressionNode, characterEscapingDepth)

@JsName("expressionToStructureString")
fun expressionToStructureString_JS(
        expressionNode: ExpressionNode
) = expressionToStructureString(expressionNode)


//substitutions
@JsName("expressionSubstitutionFromStrings")
fun expressionSubstitutionFromStrings(
        left: String,
        right: String,
        scope: String = "",
        basedOnTaskContext: Boolean = false,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = mathhelper.seambl.api.expressionSubstitutionFromStrings(left, right, scope, basedOnTaskContext, functionConfiguration = functionConfiguration, compiledConfiguration = compiledConfiguration)

@JsName("expressionSubstitutionFromStructureStrings")
fun expressionSubstitutionFromStructureStrings_JS(
        leftStructureString: String = "",
        rightStructureString: String = "",
        basedOnTaskContext: Boolean = false,
        matchJumbledAndNested: Boolean = false,
        simpleAdditional: Boolean = false,
        isExtending: Boolean = false,
        priority: Int = 50,
        code: String = "",
        nameEn: String = "",
        nameRu: String = ""
) = expressionSubstitutionFromStructureStrings(
        leftStructureString,
        rightStructureString,
        basedOnTaskContext,
        matchJumbledAndNested,
        simpleAdditional,
        isExtending,
        priority,
        code,
        nameEn,
        nameRu
)

@JsName("findApplicableSubstitutionsInSelectedPlace")
fun findApplicableSubstitutionsInSelectedPlace_JS(
        expression: ExpressionNode,
        selectedNodeIds: Array<Int>,
        compiledConfiguration: CompiledConfiguration,
        simplifyNotSelectedTopArguments: Boolean = false,
        withReadyApplicationResult: Boolean = true
) = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfiguration, simplifyNotSelectedTopArguments, withReadyApplicationResult)

@JsName("findSubstitutionPlacesInExpression")
fun findSubstitutionPlacesInExpression(
        expression: ExpressionNode,
        substitution: ExpressionSubstitution
) = mathhelper.seambl.api.findSubstitutionPlacesInExpression(expression, substitution)

@JsName("applySubstitution")
fun applySubstitution(
        expression: ExpressionNode,
        substitution: ExpressionSubstitution,
        substitutionPlaces: List<SubstitutionPlace> //containsPointersOnExpressionPlaces
) = mathhelper.seambl.api.applySubstitution(expression, substitution, substitutionPlaces)

//check solution
@JsName("checkSolutionInTex")
fun checkSolutionInTex_JS(
        originalTexSolution: String, //string with learner solution in Tex format

        //// individual task parameters:
        startExpressionIdentifier: String = "", //Expression, from which learner need to start the transformations
        targetFactPattern: String = "", //Pattern that specify criteria that learner's answer must meet
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here, that can be used as rules only for this task

        endExpressionIdentifier: String = "", //Expression, which learner need to deduce
        targetFactIdentifier: String = "", //Fact that learner need to deduce. It is more flexible than startExpressionIdentifier and endExpressionIdentifier, allow to specify inequality like '''EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{<=}{(/(2;sin(x)))}'''
        comparisonSign: String = "", //Comparison sign

        //// general configuration parameters
        //functions, which null-weight transformations allowed (if no other transformations), split by configSeparator
        //choose one of 2 api forms:
        wellKnownFunctions: List<FunctionIdentifier> = listOf(),
        wellKnownFunctionsString: String = "${configSeparator}0$configSeparator${configSeparator}1$configSeparator+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1$configSeparator^$configSeparator-1",

        //functions, which null-weight transformations allowed with any other transformations, split by configSeparator
        //choose one of 2 api forms:
        unlimitedWellKnownFunctions: List<FunctionIdentifier> = wellKnownFunctions,
        unlimitedWellKnownFunctionsString: String = wellKnownFunctionsString,

        //expression transformation rules
        //choose one of api forms:
        expressionTransformationRules: List<ExpressionSubstitution> = listOf(), //full list of expression transformations rules
        expressionTransformationRulesString: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        rulePacks: Array<String> = listOf<String>().toTypedArray(),

        maxExpressionTransformationWeight: String = "1.0",
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        scopeFilter: String = "", //subject scopes which user representation sings is used

        shortErrorDescription: String = "0" //make error message shorter and easier to understand: crop parsed steps from error description
) = checkSolutionInTex(
        originalTexSolution,

        startExpressionIdentifier,
        targetFactPattern,
        additionalFactsIdentifiers,

        endExpressionIdentifier,
        targetFactIdentifier,
        comparisonSign,

        wellKnownFunctions,
        wellKnownFunctionsString,

        unlimitedWellKnownFunctions,
        unlimitedWellKnownFunctionsString,

        expressionTransformationRules,
        expressionTransformationRulesString,
        taskContextExpressionTransformationRules,
        rulePacks,

        maxExpressionTransformationWeight,
        maxDistBetweenDiffSteps,
        scopeFilter,

        shortErrorDescription
)

@JsName("checkSolutionInTexWithCompiledConfiguration")
fun checkSolutionInTexWithCompiledConfiguration_JS(
        originalTexSolution: String, //string with learner solution in Tex format
        compiledConfiguration: CompiledConfiguration,

        //// individual task parameters:
        startExpressionIdentifier: String = "", //Expression, from which learner need to start the transformations
        targetFactPattern: String = "", //Pattern that specify criteria that learner's answer must meet
        comparisonSign: String,
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here, that can be used as rules only for this task

        endExpressionIdentifier: String = "", //Expression, which learner need to deduce
        targetFactIdentifier: String = "", //Fact that learner need to deduce. It is more flexible than startExpressionIdentifier and endExpressionIdentifier, allow to specify inequality like '''EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{<=}{(/(2;sin(x)))}'''

        shortErrorDescription: String = "0" //make error message shorter and easier to understand: crop parsed steps from error description
): TexVerificationResult {
    return checkSolutionInTexWithCompiledConfiguration(
            originalTexSolution,
            compiledConfiguration,
            startExpressionIdentifier,
            targetFactPattern,
            comparisonSign,
            additionalFactsIdentifiers,
            endExpressionIdentifier,
            targetFactIdentifier,
            shortErrorDescription)
}

@JsName("getParsedExpressionByMathML")
fun getParsedExpressionByMathML(mathML: String): String {
    val expressionTreeParser = ExpressionTreeParser(mathML)
    expressionTreeParser.parse()
    val root = expressionTreeParser.root
    return root.toString()
}

@JsName("checkFactsInMathML")
fun checkFactsInMathML(
        brushedMathML: String,
        wellKnownFunctions: String = "+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1", //functions, which null-weight transformations allowed (if no other transformations), split by configSeparator
        expressionTransformationRules: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        targetFactIdentifier: String = "", //Fact that learner need to prove should be here
        targetVariablesNames: String = "", //Variables expressions for which learner need to deduce, split by configSeparator
        minNumberOfMultipliersInAnswer: String = "", //For factorization tasks
        maxNumberOfDivisionsInAnswer: String = "", //For fraction reducing tasks
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here
        maxExpressionTransformationWeight: String = "1.0",
        unlimitedWellKnownFunctions: String = wellKnownFunctions, //functions, which null-weight transformations allowed with any other transformations, split by configSeparator
        shortErrorDescription: String = "0", //crop parsed steps from error description
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        allowedVariablesNames: String = "", //Variables expressions for which learner need to deduce, split by configSeparator
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        forbiddenFunctions: String = "", //functions cannot been used in answer
        scopeFilter: String = "", //subject scopes which user representation sings is used
        makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts: String = "0" // if true, it is possible to define custom condition to enrich known facts for transformations
) = mathhelper.seambl.mainpoints.checkFactsInMathML(
        brushedMathML,
        wellKnownFunctions,
        expressionTransformationRules,
        targetFactIdentifier,
        targetVariablesNames,
        minNumberOfMultipliersInAnswer,
        maxNumberOfDivisionsInAnswer,
        additionalFactsIdentifiers,
        maxExpressionTransformationWeight,
        unlimitedWellKnownFunctions,
        shortErrorDescription,
        taskContextExpressionTransformationRules,
        allowedVariablesNames,
        maxDistBetweenDiffSteps,
        forbiddenFunctions,
        scopeFilter,
        makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts
)

@JsName("getUserLogInPlainText")
fun getUserLogInPlainText() = log.getLogInPlainText(MessageType.USER)

@JsName("getUserLogInJson")
fun getUserLogInJson() = log.getLogInJson(MessageType.USER)

@JsName("getAllLogInPlainText")
fun getAllLogInPlainText() = log.getLogInPlainText()

@JsName("getAllLogInJson")
fun getAllLogInJson() = log.getLogInJson()

@JsName("encryptSolution")
fun encryptSolution(solution: String) = "${solution.hashCode()}_${solution.length}"

@JsName("encryptResult")
fun encryptResult(result: String, solution: String) = "$result ${solution.hashCode()}_${solution.length}"


@JsName("decodeUrlSymbols")
fun decodeUrlSymbols_JS(string: String) = decodeUrlSymbols(string)


//compare expressions without substitutions
@JsName("compareWithoutSubstitutions")
fun compareWithoutSubstitutions(
        left: ExpressionNode,
        right: ExpressionNode,
        scope: Set<String> = setOf(""),
        notChangesOnVariablesFunction: Set<String> = setOf("+", "-", "*", "/", "^"),
        maxExpressionBustCount: Int = 4096,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(scope, notChangesOnVariablesFunction),
        comparisonSettings: ComparisonSettings = ComparisonSettings().apply { this.maxExpressionBustCount = maxExpressionBustCount},
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = mathhelper.seambl.api.compareWithoutSubstitutions(left, right, scope, notChangesOnVariablesFunction, maxExpressionBustCount, functionConfiguration, comparisonSettings, DebugOutputMessages(), compiledConfiguration)